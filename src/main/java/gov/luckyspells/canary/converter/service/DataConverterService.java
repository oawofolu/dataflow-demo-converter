package gov.luckyspells.canary.converter.service;

public interface DataConverterService {
	public String convert(String data);
}
