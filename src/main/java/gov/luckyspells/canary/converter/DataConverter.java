package gov.luckyspells.canary.converter;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.handler.annotation.SendTo;

import gov.luckyspells.canary.converter.service.DataConverterService;

@EnableBinding(Processor.class)
public class DataConverter {
	
	@Autowired DataConverterService converterService;
	
	@StreamListener(Processor.INPUT)
	@SendTo(Processor.OUTPUT)
	public String transform(String data) {
		return converterService.convert(data);
	}
	
	public InputStream transformInputStream(InputStream data) {
		return null;
	}
}
