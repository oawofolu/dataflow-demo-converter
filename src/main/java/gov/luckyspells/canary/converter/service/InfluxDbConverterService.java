package gov.luckyspells.canary.converter.service;

import org.springframework.stereotype.Service;

@Service
public class InfluxDbConverterService implements DataConverterService{
	
	private static final String COMMA = ",";

	@Override
	public String convert(String data) {
		String[] tokens = data.split(COMMA);
		
		// Expected input= <timestamp>,<usage>
		// Expected output = cpu usage=<usage> <timestamp>
		return String.format("cpu usage=%s %s", tokens[1], tokens[0]);
	}
	
}
